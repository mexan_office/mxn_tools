<?php


/***************************************************************
 * Extension Manager/Repository config file for ext: "mxn_tools"
 *
 * Generated  2019-04-12
 *
 ***************************************************************/


$EM_CONF[$_EXTKEY] = array(
	'title' => 'mexan AG tools',
	'description' => 'Tools for TYPO3 instalation',
	'category' => 'misc',
	'author' => 'mexan',
	'author_email' => 'info@mexan.ch',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '2.0.1',
	'constraints' => array(
		'depends' => array(
			'typo3' => '9.5.0-10.4.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
			'mxn_template' => '',
		),
	),
	'autoload' => [
				'psr-4' => [
						'Mexan\\MxnTools\\' => 'Classes'
				],
		],
);
