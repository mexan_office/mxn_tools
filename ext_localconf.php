<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}


//add ViewHelpers:
//shared ViewHelpers for alle mexan project:
$GLOBALS['TYPO3_CONF_VARS']['SYS']['fluid']['namespaces']['mxn'][] = 'Mexan\\MxnTools\\ViewHelpers';

// EXTEND BACKEND PREVIEW INFOS
if (!$GLOBALS['TYPO3_CONF_VARS']['SYS']['features']['fluidBasedPageModule']) {
	//Only load the Hook if fluidBasedPageModule is disabled
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawFooter'][] = \Mexan\MxnTools\Hooks\Backend\PageLayoutViewEnrichmentFooter::class;
}

// Optimizes jpg and PNG files in processed folders
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Mexan\\MxnTools\\Task\\OptimizerTask'] = array(
    'extension'        => 'mxn_tools',
    'title'            => 'Bild-Optimierung',
    'description'      => 'Optimiert jpg und png-Dateien in processed-Ordner'
);
