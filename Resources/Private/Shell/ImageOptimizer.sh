#!/bin/sh

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
ROOTDIR=$(dirname $(dirname $(dirname $(dirname $(dirname $(dirname "$SCRIPTPATH"))))))

IMAGEPATH="$ROOTDIR/fileadmin/_processed_/"

find $IMAGEPATH -type f \( -name "*.jpg" -o -name "*.JPG" \) -exec jpegoptim --max=85 --strip-all {} \;
find $IMAGEPATH -type f \( -name "*.png" -o -name "*.PNG" \) -print0 | xargs -0 optipng -o7
echo "some data for the file" >> foo.txt