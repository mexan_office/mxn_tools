<?php

namespace Mexan\MxnTools\Hooks\Backend;

use TYPO3\CMS\Backend\View\PageLayoutViewDrawFooterHookInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class PageLayoutViewEnrichmentFooter implements PageLayoutViewDrawFooterHookInterface
{

    /**
     * Preprocesses the preview footer rendering of a content element.
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
     * @param string $info Processed values
     * @param array $row Record row of tt_content
     * @return void
     */
    public function preProcess(\TYPO3\CMS\Backend\View\PageLayoutView &$parentObject, &$info, array &$row)
    {
        $info = array_filter($info);

        if($row['layout']){
            $info[] = '<strong>Layout</strong> <span class="layout-backend ' . $row['layout'] . '">' . $row['layout'] . '</span>';
        }
    }
}
