<?php
namespace Mexan\MxnTools\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractConditionViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class ShortStartEndDateViewHelper extends AbstractViewHelper {
    /**
     * Arguments Initialization
     */
    // 	<mxn:ShortStartEndDate startDate="{newsItem.datetime}" endDate="{newsItem.enddate}" />

    public function initializeArguments() {
        $this->registerArgument('startDate', 'DateTime', 'DateTimeInterface object');
        $this->registerArgument('endDate', 'DateTime', 'DateTimeInterface object');
    }
    public function render() {
        return $this->NiceRange($this->arguments['startDate'], $this->arguments['endDate']);
    }

    /**
     * Returns the shortest possible date range between 2 date
     *
     * This method will build up the minimum required string to fully convey the
     * dates, eg. it will not duplicate the month name if both dates start and end
     * in the same month.
     *
     * code from : https://gist.github.com/trovster/2252907
     *
     *  Ex: Dienstag - Donnerstag, 29.– 31. Mai 2018
     *
     */
    public function NiceRange($start_date, $end_date) {

      //$start_datetime = time();
      $end_datetime = 0;
      if (!is_null($start_date)) {
        $start_datetime = $start_date->getTimestamp();
      }else{
        //error no valide datetime:
        return "";
      }
      if (!is_null($end_date)) {
        $end_datetime = $end_date->getTimestamp();
      }

        $output = '';
        //if the end_datetime is not set only show the start date:
        if ($end_datetime == 0 || strftime('%e-%B-%Y', $start_datetime) == strftime('%e-%B-%Y', $end_datetime)) {
            // same day
            $output = strftime('%A, %e. %B %Y', $start_datetime);
        } elseif (strftime('%B %Y', $start_datetime) === strftime('%B %Y', $end_datetime)) {
            // same year and month
            $output = sprintf('%s - %s, %s– %s %s', strftime('%A', $start_datetime), strftime('%A', $end_datetime), strftime('%e.', $start_datetime), strftime('%e.', $end_datetime), strftime('%B %Y', $start_datetime));
        } elseif (strftime('%Y', $start_datetime) === strftime('%Y', $end_datetime)) {
            // same year
            $output = sprintf('%s - %s, %s– %s %s', strftime('%A', $start_datetime), strftime('%A', $end_datetime), strftime('%e. %B', $start_datetime), strftime('%e. %B', $end_datetime), strftime('%Y', $start_datetime));
        } else {
            // completely different
            $output = sprintf('%s - %s, %s– %s', strftime('%A', $start_datetime), strftime('%A', $end_datetime), strftime('%e. %B %Y', $start_datetime), strftime('%e. %B %Y', $end_datetime));
        }
        return $output;
    }
}
